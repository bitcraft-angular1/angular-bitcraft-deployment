/*global angular */
'use strict';

angular.
    module('bitcraft-distribution.list').
    component('bitcraftDistributionList', {
        templateUrl: 'js/distribution-management/distribution-list/distribution-list.template.html',
        controller: function DistributionListController() {
            var self = this;

            self.$onChanges = function (changesObj) {
                if (changesObj.distributionList) {
                    self.distributionList = angular.copy(changesObj.distributionList.currentValue);
                }
            };

            /**
             * @param {{version: string}} data
             * @param {DistributionInfo} distribution
             */
            self.updateDistribution = function (data, distribution) {
                self.onUpdateDistribution({
                    distributionId: distribution._id,
                    version: data.version
                });
            };

        },
        bindings: {
            distributionList: '<',
            onRemove: '&',
            onDownload: '&',
            onCheckVersion: '&',
            onUpdateDistribution: '&'
        }
    });
