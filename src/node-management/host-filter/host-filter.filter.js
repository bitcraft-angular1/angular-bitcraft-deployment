/*global angular*/
'use strict';

angular.
    module('bitcraft-host.filter', []).
    filter('bitcraftHostFilter', function () {
        return function (input, param) {
            /** @type {Array.<HostInfo>} */
            var hostList = input;
            /** @type {string} */
            var filterCategory = param;
            /** @type {Array.<HostInfo>} */
            var result = [];

            angular.forEach(hostList, function (host) {
                if (host.category === filterCategory) {
                    result.push(host);
                }
            });

            return result;
        };
    });