/*global angular */
'use strict';

// 3rd party
var async = require('async');

/** @enum {string} */
var STATES = {
    NOT_INSTALLED: 'AGENTS.NOT_INSTALLED',
    RUNNING: 'AGENTS.RUNNING',
    DOWN: 'AGENTS.DOWN',
    PENDING: 'AGENTS.PENDING'
};

angular.module('bitcraft-node.list').
    component('bitcraftNodeList', {
        templateUrl: 'js/node-management/node-list/node-list.template.html',
        controller: [
            'Notification',
            function NodeListController(Notification) {
                var i;
                var self = this;

                self.distributionList = [];
                self.selectedDistribution = {};
                self.aceSettings =
                {
                    mode: 'javascript',
                    showGutter: false
                };

                function init() {
                    async.forEach(self.nodeList,
                        function (item, next) {
                            self.checkAgentInstall(item, function (err) {
                                self.checkState(item);
                                item.customDataAsString = JSON.stringify(item.customData, null, '\t');
                                next(err);
                            });
                        },
                        /** @param {string} err */
                        function (err) {
                            if (err) {
                                Notification.error({
                                    message: 'Failed to initialize agents (' + err + ')'
                                });
                            }
                        });
                }

                function getAgentByUuid(id) {
                    for (i = 0; i < self.nodeList.length; i += 1) {
                        if (self.nodeList[i]._id === id) {
                            return self.nodeList[i];
                        }
                    }
                }

                self.checkState = function (item) {
                    if (item.agentInstalled) {
                        item.state = STATES.PENDING;
                        self.onCheckAgentState({nodeInfo: item}).then(
                            /** @param {{data: {running: boolean}}} */
                            function (resp) {
                                item.state = resp.data.running ? STATES.RUNNING : STATES.DOWN;
                            },
                            /** @param {{data: {error: string}}} */
                            function (resp) {
                                var errorMessage = resp.statusText +
                                    (resp.data && resp.data.error && (': ' + resp.data.error));

                                Notification.error({
                                    message: 'Failed to check state (' + errorMessage + ')'
                                });
                            }
                        );
                    }
                };

                self.$onChanges = function (changesObj) {
                    if (changesObj.nodeList) {
                        self.nodeList = angular.copy(changesObj.nodeList.currentValue);
                        if (self.nodes !== undefined || !changesObj.nodeList.isFirstChange()) {
                            init();
                        }
                    }
                    if (changesObj.distributionList) {
                        self.distributionList = angular.copy(
                            changesObj.distributionList.currentValue
                        );
                        if (self.distributionList && self.distributionList.length > 0) {
                            self.selectedDistribution = self.distributionList[0];
                        }
                    }
                };

                /**
                 * @param {{
                 *      name: string,
                 *      udpPort: string,
                 *      tcpPort: string,
                 *      adminHost: string,
                 *      adminPort: string,
                 *      customData: string}} data
                 * @param {NodeInfo} node
                 */
                self.updateNode = function (data, node) {
                    /** @type {JSON} */
                    var customDataAsJson;

                    try {
                        customDataAsJson = JSON.parse(data.customData);
                    } catch (e) {
                        Notification.error({
                            message: 'Invalid JSON format in custom data (' + e + ')'
                        });
                        return;
                    }

                    console.log('customData: ' + JSON.stringify(customDataAsJson));

                    self.onUpdateNode({
                        nodeId: node._id,
                        name: data.name,
                        udpPort: parseInt(data.udpPort, 10),
                        tcpPort: parseInt(data.tcpPort, 10),
                        adminHost: data.adminHost,
                        adminPort: parseInt(data.adminPort, 10),
                        customData: customDataAsJson
                    });
                };

                /**
                 * @param {NodeInfo} item
                 * @param {function(string=)} callback
                 */
                self.checkAgentInstall = function (item, callback) {
                    item.agentInstalled = false;
                    item.state = STATES.NOT_INSTALLED;
                    self.onCheckAgentInstall({nodeInfo: item}).then(
                        function (data) {
                            item.agentInstalled = data.installed;
                            callback();
                        },
                        function (resp) {
                            Notification.error({
                                message: 'Failed to check agent state (' +
                                    resp.statusText + ': ' + resp.data + ')'
                            });
                            callback(resp.data);
                        }
                    );
                };

                /**
                 * @param {DistributionInfo} data
                 * @param {NodeInfo} node
                 */
                self.installAgent = function (data, node) {
                    self.isOperationInProgress = true;
                    self.onInstallAgent({
                        nodeId: node._id,
                        distributionId: data._id,
                        callback: function () {
                            self.isOperationInProgress = false;
                            self.checkAgentInstall(node, function () {
                                node.state = STATES.DOWN;
                            });
                        }
                    });
                };

                /** @param {NodeInfo} node */
                self.uninstallAgent = function (node) {
                    self.isOperationInProgress = true;
                    self.onUninstallNode({
                        nodeInfo: node,
                        callback: function () {
                            self.isOperationInProgress = false;
                        }
                    });
                };

                /** @param {NodeInfo} nodeInfo */
                self.startNode = function (nodeInfo) {
                    self.onStartNode({
                        nodeInfo: nodeInfo,
                        callback: function () {
                            self.checkState(nodeInfo);
                        }
                    });
                };

                /** @param {NodeInfo} nodeInfo */
                self.stopNode = function (nodeInfo) {
                    self.onStopNode({
                        nodeInfo: nodeInfo,
                        callback: function () {
                            self.checkState(nodeInfo);
                        }
                    });
                };

                /**
                 * Returns true if the node can be started
                 * @param {NodeInfo} data
                 * @returns {boolean}
                 */
                self.canStart = function (data) {
                    var agent = getAgentByUuid(data._id);
                    if (!agent) {
                        return false;
                    }

                    return agent.agentInstalled && agent.state === STATES.DOWN;
                };

                /**
                 * Returns true if the node can be stopped
                 * @param {NodeInfo} data
                 * @returns {boolean}
                 */
                self.canStop = function (data) {
                    var agent = getAgentByUuid(data._id);
                    if (!agent) {
                        return false;
                    }

                    return agent.agentInstalled && agent.state === STATES.RUNNING;
                };
            }],
        bindings: {
            nodeList: '<',
            isOperationInProgress: '=',
            distributionList: '<',
            onCheckAgentState: '&',
            onUninstallNode: '&',
            onUpdateNode: '&',
            onInstallAgent: '&',
            onCheckName: '&',
            onStartNode: '&',
            onStopNode: '&',
            onCheckAgentInstall: '&'
        }
    });
