/*global angular*/
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-node-management').
    component('bitcraftNodeManagement', {
        templateUrl: 'js/node-management/node-management.template.html',
        controller: [
            '$log', 'dialogs', 'Notification', 'DeploymentManagement', 'ServerConfig',
            function nodeManagementController(
                $log,
                dialogs,
                Notification,
                DeploymentManagement,
                ServerConfig
            ) {
                var self = this;

                /** @type {boolean} */
                self.isOperationInProgress = false;

                self.selectedDistribution = {};
                self.hostList = [];
                self.nodeList = [];
                self.newNodeName = '';
                self.newUdpPort = '';
                self.newTcpPort = '';
                /** @type {{host: string, port: string}} */
                self.adminAccessHost = '0.0.0.0';
                self.adminAccessPort = '';
                self.hostCategory = 'agent';
                self.selectedHost = undefined;
                //TODO load custom data
                self.customData = '';
                /** @type {SystemConfig} */
                self.config = {};
                self.aceSettings =
                {
                    mode: 'javascript'
                };

                ServerConfig.getCurrentConfig().then(function (data) {
                    self.config = data;

                    self.newUdpPort = self.config.nodeManagement.udpPort;
                    self.newTcpPort = self.config.nodeManagement.tcpPort;
                    self.adminAccessHost = self.config.nodeManagement.adminHost;
                    self.adminAccessPort = self.config.nodeManagement.adminPort;
                    self.customData = JSON.stringify(self.config.nodeManagement.customData, null, '\t');
                }, function (resp) {
                    $log.debug('failed to retrieve config from server. (' + resp.data + ')');
                });

                function refreshNodes() {
                    if (self.selectedHost) {
                        DeploymentManagement.listNodes(self.selectedHost).then(
                            function (data) {
                                self.nodeList = data;
                            },
                            function (resp) {
                                Notification.error({
                                    message: 'Failed to retrieve nodes (' + resp.statusText + ')'
                                });
                            }
                        );
                    }
                }

                function refreshHosts() {
                    DeploymentManagement.listHosts().then(function (data) {
                        self.hostList = data;
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to retrieve hosts (' + resp.statusText + ')'
                        });
                    });
                }

                /**
                 * @param {Array.<DistributionInfo>} distributionList
                 * @returns {Array.<DistributionInfo>}
                 */
                function sortFilterDistributions(distributionList) {
                    if (distributionList !== undefined) {
                        return distributionList.filter(function (item) {
                            return item.category === self.hostCategory;
                        }).sort(function (a, b) {
                            return (a.uploadTimestamp > a.uploadTimestamp ? -1 : 1);
                        });
                    }
                }

                /** @param {string} category */
                self.updateHostCategory = function (category) {
                    self.hostCategory = category;
                    self.refreshDistributions();
                };

                self.refreshDistributions = function () {
                    DeploymentManagement.listDistributions().then(function (data) {
                        self.distributions = sortFilterDistributions(data);
                        if (self.distributions && self.distributions.length > 0) {
                            self.selectedDistribution = self.distributions[0];
                        }
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to retrieve distributions (' + resp.statusText + ')'
                        });
                    });
                };

                /** @param {HostInfo} hostInfo */
                self.onHostClicked = function (hostInfo) {
                    if (self.selectedHost === hostInfo) {
                        return;
                    }

                    //TODO highlight selected host
                    self.selectedHost = hostInfo;
                    refreshNodes();
                };

                self.onAddNode = function () {
                    /** @type {JSON} */
                    var customDataAsJson;

                    try {
                        customDataAsJson = JSON.parse(self.customData);
                    } catch (e) {
                        Notification.error({
                            message: 'Invalid JSON format in custom data (' + e + ')'
                        });
                        return;
                    }

                    DeploymentManagement.addNode(
                        self.selectedHost,
                        self.newNodeName,
                        parseInt(self.newUdpPort, 10),
                        parseInt(self.newTcpPort, 10),
                        self.adminAccessHost,
                        parseInt(self.adminAccessPort, 10),
                        customDataAsJson
                    ).then(function (data) {
                        self.newNodeName = '';
                        self.newUdpPort = self.config.nodeManagement.udpPort;
                        self.newTcpPort = self.config.nodeManagement.tcpPort;
                        self.adminAccessHost = self.config.nodeManagement.adminHost;
                        self.adminAccessPort = self.config.nodeManagement.adminPort;
                        refreshNodes();

                        Notification.success({
                            message: 'Node successfully added'
                        });
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to add node (' + resp.statusText + ')'
                        });
                    });
                };

                /**
                 * @param {NodeInfo} nodeInfo
                 * @param {function()} callback
                 */
                self.onUninstallNode = function (nodeInfo, callback) {
                    var dlg = dialogs.confirm(
                        'Confirmation required',
                        'Do you want to uninstall the node?',
                        {size: 'sm'}
                    );
                    dlg.result.then(function (btn) {
                        Notification.info({
                            message: 'Uninstalling node...'
                        });
                        DeploymentManagement.uninstallAgent(
                            nodeInfo
                        ).then(
                            function (resp) {
                                Notification.success({
                                    message: 'Distribution successfully uninstalled'
                                });
                                refreshNodes(self.selectedHost);
                                callback();
                            },
                            /** @param {{data: {error: string}}} */
                            function (resp) {
                                var errorMessage = resp.statusText +
                                    (resp.data && resp.data.error && (': ' + resp.data.error));
                                Notification.error({
                                    message: 'Failed to uninstall distribution (' + errorMessage + ')'
                                });
                                callback();
                            }
                        );
                    },
                        function () {
                            callback();
                        });
                };

                /** @param {HostInfo} hostInfo */
                self.onUninstallHost = function (hostInfo) {
                    DeploymentManagement.getNodeCount(hostInfo).then(
                        function (data) {
                            if (data.nodeCount > 0) {
                                dialogs.error(
                                    'Host contains nodes',
                                    'Please make sure to uninstall all nodes before uninstalling the host.',
                                    {size: 'sm'}
                                );
                                return;
                            }

                            var dlg = dialogs.confirm(
                                'Confirmation required',
                                'Do you want to uninstall the host?',
                                {size: 'sm'}
                            );
                            dlg.result.then(function (btn) {
                                Notification.info({
                                    message: 'Uninstalling host...'
                                });
                                DeploymentManagement.uninstallHost(
                                    hostInfo
                                ).then(function (data) {
                                    if (self.selectedHost === hostInfo) {
                                        self.selectedHost = null;
                                    }
                                    refreshHosts();
                                    Notification.success({
                                        message: 'Host successfully uninstalled'
                                    });
                                }, function (resp) {
                                    Notification.error({
                                        message: 'Failed to uninstall host (' + resp.statusText + ')'
                                    });
                                });
                            });
                        },
                        function (resp) {
                            Notification.error({
                                message: 'Failed to check node count (' +
                                    resp.statusText + ': ' + resp.data + ')'
                            });
                        }
                    );
                };

                /**
                 * @param {HostInfo} hostInfo
                 * @param {function()} callback
                 */
                self.onReinstallSentinel = function (hostInfo, callback) {
                    var dlg = dialogs.confirm(
                        'Confirmation required',
                        'Do you want to reinstall the sentinel?',
                        {size: 'sm'}
                    );
                    dlg.result.then(function (btn) {
                        Notification.info({
                            message: 'Reinstalling sentinel...'
                        });
                        DeploymentManagement.reinstallSentinel(
                            hostInfo
                        ).then(function (data) {
                            if (self.selectedHost === hostInfo) {
                                self.selectedHost = null;
                            }
                            refreshHosts();
                            self.nodeList = [];
                            callback();
                            Notification.success({
                                message: 'Sentinel successfully reinstalled'
                            });
                        }, function (resp) {
                            callback();
                            Notification.error({
                                message: 'Failed to reinstall sentinel (' + resp.statusText + ')'
                            });
                        });
                    },
                        function () {
                            callback();
                        });
                };

                /**
                 * @param {string} nodeId
                 * @param {string} name
                 * @param {number} udpPort
                 * @param {number} tcpPort
                 * @param {string} adminHost
                 * @param {number} adminPort
                 * @param {JSON} customData
                 */
                self.onUpdateNode = function (nodeId, name, udpPort, tcpPort, adminHost, adminPort, customData) {
                    DeploymentManagement.updateNode(
                        nodeId,
                        name,
                        udpPort,
                        tcpPort,
                        adminHost,
                        adminPort,
                        customData
                    ).then(function (data) {
                        Notification.success({
                            message: 'Node successfully updated'
                        });
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to updated node (' + resp.statusText + ')'
                        });
                    });
                };

                /**
                 * @param {string} nodeId
                 * @param {string} distributionId
                 * @param {function()} callback
                 */
                self.onInstallAgent = function (nodeId, distributionId, callback) {
                    Notification.info({
                        message: 'Installing agent...'
                    });
                    DeploymentManagement.installAgent(
                        nodeId,
                        distributionId
                    ).then(
                        function (resp) {
                            Notification.success({
                                message: 'Distribution successfully installed'
                            });
                            callback();
                        },
                        /** @param {{data: {error: string}}} */
                        function (resp) {
                            var errorMessage = resp.statusText +
                                (resp.data && resp.data.error && (': ' + resp.data.error));
                            Notification.error({
                                message: 'Failed to install distribution (' + errorMessage + ')'
                            });
                            callback();
                        }
                    );
                };

                /**
                 * @param {string} distributionId
                 * @param {string} hostId
                 * @param {function()} callback
                 */
                self.onInstallHttp = function (distributionId, hostId, callback) {
                    Notification.info({
                        message: 'Installing http...'
                    });
                    DeploymentManagement.installHttp(
                        distributionId,
                        hostId
                    ).then(function (data) {
                        Notification.success({
                            message: 'Http successfully installed'
                        });
                        callback();
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to install http (' + resp.statusText + ')'
                        });
                        callback();
                    });
                };

                /**
                 * @param {HostInfo} hostInfo
                 * @param {function()} callback
                 */
                self.onStartHttp = function (hostInfo, callback) {
                    DeploymentManagement.startHttp(
                        hostInfo._id
                    ).then(function (data) {
                        Notification.success({
                            message: 'Http successfully started'
                        });
                        callback();
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to start http (' + resp.statusText + ')'
                        });
                        callback();
                    });
                };

                /**
                 * @param {HostInfo} hostInfo
                 * @param {function()} callback
                 */
                self.onStopHttp = function (hostInfo, callback) {
                    DeploymentManagement.stopHttp(
                        hostInfo._id
                    ).then(function (data) {
                        Notification.success({
                            message: 'Http successfully stopped'
                        });
                        callback();
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to stop http (' + resp.statusText + ')'
                        });
                        callback();
                    });
                };

                /**
                 * @param {NodeInfo} nodeInfo
                 * @param {function()} callback
                 */
                self.onStartNode = function (nodeInfo, callback) {
                    DeploymentManagement.startNode(
                        nodeInfo._id
                    ).then(
                        /** @param {{data: {running: boolean}}} resp */
                        function (resp) {
                            if (!resp.data.running) {
                                Notification.error({
                                    message: 'Agent could not be started'
                                });
                            } else {
                                Notification.success({
                                    message: 'Agent successfully started'
                                });
                            }
                            callback();
                        },
                        /** @param {{data: {error: string}}} */
                        function (resp) {
                            var errorMessage = resp.statusText +
                                (resp.data && resp.data.error && (': ' + resp.data.error));
                            Notification.error({
                                message: 'Failed to start agent (' + errorMessage + ')'
                            });
                            callback();
                        }
                    );
                };

                /**
                 * @param {NodeInfo} nodeInfo
                 * @param {function()} callback
                 */
                self.onStopNode = function (nodeInfo, callback) {
                    DeploymentManagement.stopNode(
                        nodeInfo._id
                    ).then(
                        /** @param {{data: {running: boolean}}} resp */
                        function (resp) {
                            if (resp.data.running) {
                                Notification.error({
                                    message: 'Agent could not be stopped'
                                });
                            } else {
                                Notification.success({
                                    message: 'Agent successfully stopped'
                                });
                            }
                            callback();
                        },
                        /** @param {{data: {error: string}}} */
                        function (resp) {
                            var errorMessage = resp.statusText +
                                (resp.data && resp.data.error && (': ' + resp.data.error));
                            Notification.error({
                                message: 'Failed to stop agent (' + errorMessage + ')'
                            });
                            callback();
                        }
                    );
                };

                /** @param {HostInfo} hostInfo */
                self.stopAllNodes = function (hostInfo) {
                    self.isOperationInProgress = true;
                    DeploymentManagement.stopAllNodes(
                        hostInfo._id
                    ).then(
                        /** @param {{data: {allAgentsStopped: boolean}}} resp */
                        function (resp) {
                            if (!resp.data.allAgentsStopped) {
                                Notification.error({
                                    message: 'Not all agents could be stopped'
                                });
                            } else {
                                Notification.success({
                                    message: 'All agents were successfully stopped'
                                });
                            }
                            refreshNodes(self.selectedHost);
                            self.isOperationInProgress = false;
                        },
                        /** @param {{data: string}} */
                        function (resp) {
                            var errorMessage = resp.statusText + (resp.data && (': ' + resp.data));
                            Notification.error({
                                message: 'Failed to stop all agents (' + errorMessage + ')'
                            });
                            refreshNodes(self.selectedHost);
                            self.isOperationInProgress = false;
                        }
                    );
                };

                /** @param {HostInfo} hostInfo */
                self.startAllNodes = function (hostInfo) {
                    self.isOperationInProgress = true;
                    DeploymentManagement.startAllNodes(
                        hostInfo._id
                    ).then(
                        /** @param {{data: {allAgentsStarted: boolean}}} resp */
                        function (resp) {
                            if (!resp.data.allAgentsStarted) {
                                Notification.error({
                                    message: 'Not all agents could be started'
                                });
                            } else {
                                Notification.success({
                                    message: 'All agents were successfully started'
                                });
                            }
                            refreshNodes(self.selectedHost);
                            self.isOperationInProgress = false;
                        },
                        /** @param {{data: string}} */
                        function (resp) {
                            var errorMessage = resp.statusText + (resp.data && (': ' + resp.data));
                            Notification.error({
                                message: 'Failed to start all agents (' + errorMessage + ')'
                            });
                            refreshNodes(self.selectedHost);
                            self.isOperationInProgress = false;
                        }
                    );
                };

                /**
                 * @param {HostInfo} hostInfo
                 * @param {DistributionInfo} distributionInfo
                 */
                self.installAllNodes = function (hostInfo, distributionInfo) {
                    self.isOperationInProgress = true;
                    Notification.info({
                        message: 'Installing all nodes...'
                    });
                    DeploymentManagement.installAllNodes(
                        hostInfo._id,
                        distributionInfo._id
                    ).then(
                        function (resp) {
                            Notification.success({
                                message: 'All agents were successfully installed'
                            });
                            refreshNodes(self.selectedHost);
                            self.isOperationInProgress = false;
                        },
                        /** @param {{data: {error: string}}} */
                        function (resp) {
                            var errorMessage = resp.statusText +
                                (resp.data && resp.data.error && (': ' + resp.data.error));

                            Notification.error({
                                message: 'Failed to install all agents (' + errorMessage + ')'
                            });
                            refreshNodes(self.selectedHost);
                            self.isOperationInProgress = false;
                        }
                    );
                };

                /** @param {HostInfo} hostInfo */
                self.uninstallAllNodes = function (hostInfo) {
                    var dlg = dialogs.confirm(
                        'Confirmation required',
                        'Do you want to uninstall all nodes?',
                        {size: 'sm'}
                    );
                    dlg.result.then(function (btn) {
                        self.isOperationInProgress = true;
                        Notification.info({
                            message: 'Uninstalling all nodes...'
                        });
                        DeploymentManagement.uninstallAllNodes(
                            hostInfo._id
                        ).then(
                            function (resp) {
                                Notification.success({
                                    message: 'All nodes were successfully uninstalled'
                                });
                                refreshNodes(self.selectedHost);
                                self.isOperationInProgress = false;
                            },
                            /** @param {{data: {error: string}}} */
                            function (resp) {
                                var errorMessage = resp.statusText +
                                    (resp.data && resp.data.error && (': ' + resp.data.error));

                                Notification.error({
                                    message: 'Failed to uninstall all nodes (' + errorMessage + ')'
                                });
                                refreshNodes(self.selectedHost);
                                self.isOperationInProgress = false;
                            }
                        );
                    });
                };

                self.onCheckName = DeploymentManagement.checkAgentName;
                self.onCheckAgentState = DeploymentManagement.checkAgentState;
                self.checkHttpState = DeploymentManagement.checkHttpState;
                self.onCheckHttpInstall = DeploymentManagement.onCheckHttpInstall;
                self.onCheckAgentInstall = DeploymentManagement.onCheckAgentInstall;
                self.onGetNodeCount = DeploymentManagement.getNodeCount;

                refreshHosts();
                self.refreshDistributions();
            }
        ]
    });


/**
 * @typedef {Object} AdminAccess
 * @property {string} host
 * @property {number} port
 */

/**
 * @typedef {Object} NodeInfo
 * @property {ObjectID|string} _id
 * @property {ObjectID|string} hostId
 * @property {string} uuid
 * @property {string} name
 * @property {number} udpPort
 * @property {number} tcpPort
 * @property {AdminAccess} adminAccess
 * @property {number} bootTime
 * @property {string} distributionId
 * @property {string} distributionVersion
 * @property {number} distributionTimestamp
 * @property {number} installTimestamp
 * @property {Object} customData
 */

/**
 * @typedef {Object} DistributionInfo
 * @property {ObjectID} _id
 * @property {string} category
 * @property {string} name
 * @property {string} version
 * @property {number} uploadTimestamp
 * @property {Buffer} data
 */
