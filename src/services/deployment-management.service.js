/*global angular, window*/
'use strict';

angular.
    module('bitcraft-deployment-management').
    factory('DeploymentManagement', [
        '$http', '$log', '$q', 'Upload',
        function ($http, $log, $q, Upload) {

            /**
             * Upload a new distribution
             * @param {string} hostCategory
             * @param {number} version
             * @param {File} file
             */
            function uploadDistribution(hostCategory, version, file) {
                var fields = {
                    category: hostCategory,
                    name: file.name,
                    version: version,
                    file: file
                };

                return Upload.upload({
                    url: './rest/uploadDistributionArchive',
                    fields: fields
                });
            }

            /**
             * Upload a new sentinel archive
             * @param {File} file
             */
            function uploadSentinel(file) {
                var fields = {
                    file: file
                };

                return Upload.upload({
                    url: './rest/uploadSentinelArchive',
                    fields: fields
                });
            }

            /**
             * Install the sentinel for a new host
             * @param {HostInfo} hostInfo
             * @param {File} [sentinelFile]
             */
            function installHost(hostInfo, sentinelFile) {
                var fields = {
                    category: hostInfo.category,
                    sentinelPort: hostInfo.sentinelPort,
                    localAddress: hostInfo.localAddress,
                    id: hostInfo._id,
                    dns: hostInfo.dns
                };
                if (sentinelFile) {
                    fields.file = sentinelFile;
                }

                return Upload.upload({
                    url: './rest/installHost',
                    fields: fields
                });
            }

            /**
             * Get all hosts in database
             */
            function listHosts() {
                var deferred = $q.defer();

                $http.get('./rest/listHosts').then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the agent list (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Get all nodes of a specific host
             * @param {HostInfo} hostInfo
             */
            function listNodes(hostInfo) {
                var deferred = $q.defer();

                $http.get('./rest/listNodes', {
                    params: {
                        hostId: hostInfo._id
                    }
                }).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the node list (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /** Get all distributions */
            function listDistributions(hostInfo) {
                var deferred = $q.defer();

                $http.get('./rest/listDistributions').then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error(
                        'Failed to retrieve the distribution list (' + resp.statusText + ')'
                    );
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Add a new node to a host
             * @param {HostInfo} hostInfo
             * @param {string} name
             * @param {number} udpPort
             * @param {number} tcpPort
             * @param {string} adminHost
             * @param {number} adminPort
             * @param {JSON} customData
             */
            function addNode(hostInfo, name, udpPort,
                             tcpPort, adminHost, adminPort, customData) {
                var req = {
                    method: 'POST',
                    url: './rest/addNode',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        host: hostInfo,
                        name: name,
                        udpPort: udpPort,
                        tcpPort: tcpPort,
                        adminHost: adminHost,
                        adminPort: adminPort,
                        customData: customData
                    }
                };

                return $http(req);
            }

            /**
             * Remove a node from the database
             * @param {NodeInfo} nodeInfo
             */
            function uninstallAgent(nodeInfo) {
                var req = {
                    method: 'POST',
                    url: './rest/uninstallAgent',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {nodeId: nodeInfo._id}
                };

                return $http(req);
            }

            /**
             * Remove a distribution from the database
             * @param {DistributionInfo} distributionInfo
             */
            function removeDistribution(distributionInfo) {
                var req = {
                    method: 'POST',
                    url: './rest/removeDistribution',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {distributionId: distributionInfo._id}
                };

                return $http(req);
            }

            /**
             * Downlaod a distribution from the database
             * @param {DistributionInfo} distributionInfo
             */
            function downloadDistribution(distributionInfo) {
                var url = './rest/downloadDistribution?distributionId=' + distributionInfo._id;
                window.open(url, '_blank');
            }

            /**
             * Remove a host from the database
             * @param {HostInfo} hostInfo
             */
            function uninstallHost(hostInfo) {
                var req = {
                    method: 'POST',
                    url: './rest/uninstallHost',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostInfo._id}
                };

                return $http(req);
            }

            /**
             * Reinstall the latest sentinel on the host
             * @param {HostInfo} hostInfo
             */
            function reinstallSentinel(hostInfo) {
                var req = {
                    method: 'POST',
                    url: './rest/reinstallSentinel',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: hostInfo
                };

                return $http(req);
            }

            /**
             * Update a node in the database
             * @param {string} id
             * @param {string} name
             * @param {number} udpPort
             * @param {number} tcpPort
             * @param {string} adminHost
             * @param {number} adminPort
             * @param {JSON} customData
             */
            function updateNode(id, name, udpPort, tcpPort, adminHost, adminPort, customData) {
                var req = {
                    method: 'POST',
                    url: './rest/updateNode',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        id: id,
                        name: name,
                        udpPort: udpPort,
                        tcpPort: tcpPort,
                        adminHost: adminHost,
                        adminPort: adminPort,
                        customData: customData
                    }
                };

                return $http(req);
            }

            /**
             * Update a distribution in the database
             * @param {string} id
             * @param {string} version
             */
            function updateDistribution(id, version) {
                var req = {
                    method: 'POST',
                    url: './rest/updateDistribution',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {id: id, version: version}
                };

                return $http(req);
            }

            /**
             * Install a distribution for a node
             * @param {string} nodeId
             * @param {string} distributionId
             */
            function installAgent(nodeId, distributionId) {
                var req = {
                    method: 'POST',
                    url: './rest/installAgent',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {nodeId: nodeId, distributionId: distributionId}
                };

                return $http(req);
            }

            /**
             * Install a http
             * @param {string} distributionId
             * @param {string} hostId
             */
            function installHttp(distributionId, hostId) {
                var req = {
                    method: 'POST',
                    url: './rest/installHttp',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {distributionId: distributionId, hostId: hostId}
                };

                return $http(req);
            }

            /** @param {string} hostId */
            function startHttp(hostId) {
                var req = {
                    method: 'POST',
                    url: './rest/startHttp',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostId}
                };

                return $http(req);
            }

            /**
             * Start the agent of a node
             * @param {string} nodeId
             */
            function startNode(nodeId) {
                var req = {
                    method: 'POST',
                    url: './rest/startNode',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {nodeId: nodeId}
                };

                return $http(req);
            }

            /**
             * Stop the node
             * @param {string} nodeId
             */
            function stopNode(nodeId) {
                var req = {
                    method: 'POST',
                    url: './rest/stopNode',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {nodeId: nodeId}
                };

                return $http(req);
            }

            /**
             * Stop all nodes
             * @param {string} hostId
             */
            function stopAllNodes(hostId) {
                var req = {
                    method: 'POST',
                    url: './rest/stopAllNodes',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostId}
                };

                return $http(req);
            }

            /**
             * Start all nodes
             * @param {string} hostId
             */
            function startAllNodes(hostId) {
                var req = {
                    method: 'POST',
                    url: './rest/startAllNodes',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostId}
                };

                return $http(req);
            }

            /**
             * Install all nodes
             * @param {string} hostId
             * @param {string} distributionId
             */
            function installAllNodes(hostId, distributionId) {
                var req = {
                    method: 'POST',
                    url: './rest/installAllNodes',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostId, distributionId: distributionId}
                };

                return $http(req);
            }

            /**
             * Uninstall all nodes
             * @param {string} hostId
             */
            function uninstallAllNodes(hostId) {
                var req = {
                    method: 'POST',
                    url: './rest/uninstallAllNodes',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostId}
                };

                return $http(req);
            }

            /** @param {string} hostId */
            function stopHttp(hostId) {
                var req = {
                    method: 'POST',
                    url: './rest/stopHttp',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {hostId: hostId}
                };

                return $http(req);
            }

            /**
             * Checks the state of the http
             * @param {HostInfo} hostInfo
             */
            function checkHttpState(hostInfo) {
                var deferred = $q.defer();

                $http.get('./rest/checkHttpState', {
                    params: {
                        hostId: hostInfo._id
                    }
                }).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the host status (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Checks the state of the http
             * @param {HostInfo} hostInfo
             */
            function onCheckHttpInstall(hostInfo) {
                var deferred = $q.defer();
                $http.get('./rest/checkHttpInstall', {
                    params: {
                        hostId: hostInfo._id
                    }
                }).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the http status (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Checks the state of the agent
             * @param {NodeInfo} nodeInfo
             */
            function onCheckAgentInstall(nodeInfo) {
                var deferred = $q.defer();
                $http.get('./rest/checkAgentInstall', {
                    params: {
                        nodeId: nodeInfo._id
                    }
                }).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the agent status (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Checks the state of the agent
             * @param {NodeInfo} nodeInfo
             */
            function checkAgentState(nodeInfo) {
                var deferred = $q.defer();

                $http.get('./rest/checkAgentState', {
                    params: {
                        nodeId: nodeInfo._id
                    }
                }).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the agent status (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Get the node count of a host
             * @param {HostInfo} hostInfo
             */
            function getNodeCount(hostInfo) {
                var deferred = $q.defer();

                $http.get('./rest/getNodeCount', {
                    params: {
                        hostId: hostInfo._id
                    }
                }).then(function successCallback(res) {
                    deferred.resolve(res.data);
                }, function errorCallback(resp) {
                    $log.error('Failed to retrieve the node count of host "' +
                        hostInfo._id + '" (' + resp.statusText + ')');
                    deferred.reject(resp);
                });

                return deferred.promise;
            }

            /**
             * Checks if the agent name is valid. Returns a message in case of error,
             * otherwise returns undefined.
             * @param {string|undefined} data
             * @returns {string|undefined}
             */
            function checkAgentName(data) {
                if (typeof data !== 'string' || data.length === 0) {
                    return 'Agent name cannot be empty';
                }
            }

            return {
                uploadDistribution: uploadDistribution,
                uploadSentinel: uploadSentinel,
                installHost: installHost,
                listHosts: listHosts,
                addNode: addNode,
                listNodes: listNodes,
                stopAllNodes: stopAllNodes,
                startAllNodes: startAllNodes,
                installAllNodes: installAllNodes,
                uninstallAllNodes: uninstallAllNodes,
                listDistributions: listDistributions,
                removeDistribution: removeDistribution,
                downloadDistribution: downloadDistribution,
                uninstallAgent: uninstallAgent,
                uninstallHost: uninstallHost,
                reinstallSentinel: reinstallSentinel,
                updateNode: updateNode,
                updateDistribution: updateDistribution,
                installAgent: installAgent,
                installHttp: installHttp,
                onCheckHttpInstall: onCheckHttpInstall,
                onCheckAgentInstall: onCheckAgentInstall,
                checkHttpState: checkHttpState,
                checkAgentState: checkAgentState,
                checkAgentName: checkAgentName,
                startHttp: startHttp,
                stopHttp: stopHttp,
                startNode: startNode,
                stopNode: stopNode,
                getNodeCount: getNodeCount
            };
        }
    ]);
